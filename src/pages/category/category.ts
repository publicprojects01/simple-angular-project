import {Component,ApplicationRef} from '@angular/core';
import {IonicPage, NavController, NavParams,Platform} from 'ionic-angular';
import {CommonService} from "../../common.service";
import {SmartphonePage} from "../smartphone/smartphone";
import {AdMobFree, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import {GLOBAL_CONFIG} from "../../my-config/global-config";
import {CookieService} from "ngx-cookie-service";

@IonicPage()
@Component({
  selector: 'page-category',
  templateUrl: 'category.html',
})
export class CategoryPage {
  categoryName: String;
  categoryId: String;
  smartphoneList = [];
  p = 1;

  constructor(private cookieService:CookieService,private admobFree: AdMobFree,private platform: Platform,private commonService: CommonService,private appRef:ApplicationRef, public navCtrl: NavController, public navParams: NavParams) {
    if(this.cookieService.get('banner_android').length<15){
      this.commonService.fetchAdmobInfo();
    }
    this.categoryName = this.navParams.get('categoryName');
    this.categoryId = this.navParams.get('categoryId');
    this.smartphoneListByModel(this.categoryId, 0);
    this.admobFree.banner.remove();
    this.showBanner();
  }

  smartphoneListByModel(id, page) {
    const custhis = this;
    setTimeout(function () {
      custhis.commonService.smartphoneListByModel(id, page).subscribe(response => {
         if (response.status === 200) {
          custhis.smartphoneList = JSON.parse(response['_body']);
          custhis.appRef.tick();
        }
      });
    })
  }

  next(infiniteScroll) {
    var custhis=this;
    this.commonService.smartphoneListByModel(this.categoryId, this.p).subscribe(response => {
      if (response.status === 200) {
        for (var i = 0; i < JSON.parse(response['_body']).length; ++i) {
          this.smartphoneList.push(JSON.parse(response['_body'])[i]);
        }

        custhis.appRef.tick();
      }
    }, error => {

    });
    infiniteScroll.complete();
    this.p += 1;

  }

  goSmartphone(id, name, image, ram, raminfo, battery, batteryinfo, screen, screeninfo,camera,camera_info) {
    this.navCtrl.push(SmartphonePage, {
      id: id,
      name: name,
      image: image,
      ram: ram,
      ram_info: raminfo,
      battery: battery,
      battery_info: batteryinfo,
      screen_inch: screen,
      screen_info: screeninfo,
      camera:camera,
      camera_info:camera_info
    });
  }

  showBanner() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('banner_android');
    } else if (this.platform.is('ios')) {
      adId = this.cookieService.get('banner_ios');
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare();
  }

}
