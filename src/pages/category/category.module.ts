import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoryPage } from './category';

@NgModule({
  imports: [
    IonicPageModule.forChild(CategoryPage),
  ],
})
export class CategoryPageModule {}
