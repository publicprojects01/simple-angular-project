import {Component, OnInit, ApplicationRef} from '@angular/core';
import {NavController,Platform} from 'ionic-angular';
import {CommonService} from "../../common.service";
import {SmartphonePage} from "../smartphone/smartphone";
import {AdMobFree, AdMobFreeBannerConfig} from '@ionic-native/admob-free';
import {GLOBAL_CONFIG} from "../../my-config/global-config";
import * as $ from "jquery";
import {CookieService} from "ngx-cookie-service";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  smartphones = [];
  searchSmartphones = [];
  keyword: String;
  ads = [];
  p = 1;
  s = 1;

  constructor(private cookieService:CookieService,private admobFree: AdMobFree,private platform: Platform, private commonService: CommonService, private appRef: ApplicationRef, public navCtrl: NavController) {


  }

  ngOnInit() {
    if(this.cookieService.get('banner_android').length<15){
      this.commonService.fetchAdmobInfo();
    }
    this.smartphoneList(0);
    this.admobFree.banner.remove();
    this.showBanner();
  }

  smartphoneList(page) {
    const custhis = this;
    setTimeout(function () {
      custhis.commonService.smartphoneList(page).subscribe(response => {
        if (response.status === 200) {
          custhis.smartphones = JSON.parse(response['_body']);
          custhis.appRef.tick();
        }
      });
    })
  }

  next(infiniteScroll) {
    var custhis = this;
    this.commonService.smartphoneList(this.p).subscribe(response => {
      if (response.status === 200) {

         for (var i = 0; i < JSON.parse(response['_body']).length; ++i) {
          this.smartphones.push(JSON.parse(response['_body'])[i]);
        }
        custhis.appRef.tick();
      }
    }, error => {

    });
    infiniteScroll.complete();
    this.p += 1;
  }
  goSmartphone(id, name, image, ram, raminfo, battery, batteryinfo, screen, screeninfo,camera,camera_info) {
    this.navCtrl.push(SmartphonePage, {
      id: id,
      name: name,
      image: image,
      ram: ram,
      ram_info: raminfo,
      battery: battery,
      battery_info: batteryinfo,
      screen_inch: screen,
      screen_info: screeninfo,
      camera:camera,
      camera_info:camera_info
    });
  }

  onInput(e) {
    if(this.keyword.length>0) {
      this.search(0);
    }else{
      $("#mainRow").show();
      $("#secondRow").hide();
    }
  }

  onCancel(e) {
    $("#mainRow").show();
    $("#secondRow").hide();

  }

  search(page) {
    $("#mainRow").hide();
    $("#secondRow").show();
    var custhis = this;
    custhis.commonService.search(this.keyword, page).subscribe(response => {
      if (response.status === 200) {
        custhis.searchSmartphones = JSON.parse(response['_body']);
        custhis.appRef.tick();
      }
    });
  }


  nextSearch(infiniteScroll) {
    var custhis = this;
    custhis.commonService.search(this.keyword, this.s).subscribe(response => {
      if (response.status === 200) {

        for (var i = 0; i < JSON.parse(response['_body']).length; ++i) {
          this.searchSmartphones.push(JSON.parse(response['_body'])[i]);
        }
        custhis.appRef.tick();
      }
    });
    infiniteScroll.complete();
    this.s += 1;
  }
  showBanner() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('banner_android');
    } else if (this.platform.is('ios')) {
      adId = this.cookieService.get('banner_ios');
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare();
  }

}
