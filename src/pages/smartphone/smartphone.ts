import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams,Platform} from 'ionic-angular';
import {CommonService} from "../../common.service";
import {AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig} from '@ionic-native/admob-free';
import {GLOBAL_CONFIG} from "../../my-config/global-config";
import {CookieService} from "ngx-cookie-service";

@IonicPage()
@Component({
  selector: 'page-smartphone',
  templateUrl: 'smartphone.html',
})
export class SmartphonePage {
  id: String;
  name: String;
  image: String;
  ram: String;
  ram_info: String;
  battery: String;
  battery_info: String;
  camera: String;
  camera_info: String;
  screen_inch: String;
  screen_info: String;
  categories = [];
  subCategories = [];
  details = [];

  constructor(private cookieService:CookieService,private admobFree: AdMobFree,private platform: Platform,private commonService: CommonService, public navCtrl: NavController, public navParams: NavParams) {
    if(this.cookieService.get('banner_android').length<15){
      this.commonService.fetchAdmobInfo();
    }

    //------------------ A D S --------------------------
    console.log(this.cookieService.get('adcounter'))
    if (parseInt(this.cookieService.get('adcounter')) == 15) {
      this.showReward();
      this.cookieService.set('adcounter', '1')
    } else if (parseInt(this.cookieService.get('adcounter')) % 5 == 0) {
      this.showInterstitial();
    }
    this.cookieService.set('adcounter', (parseInt(this.cookieService.get('adcounter')) + 1).toString());
    //------------------ A D S --------------------------

    this.id = this.navParams.get('id');
    this.name = this.navParams.get('name');
    this.image = this.navParams.get('image');
    this.ram = this.navParams.get('ram');
    this.ram_info = this.navParams.get('ram_info');
    this.battery = this.navParams.get('battery');
    this.battery_info = this.navParams.get('battery_info');
    this.camera = this.navParams.get('camera');
    this.camera_info = this.navParams.get('camera_info');
    this.screen_inch = this.navParams.get('screen_inch');
    this.screen_info = this.navParams.get('screen_info');

    const custhis = this;
    setTimeout(function () {
      custhis.commonService.details(custhis.id).subscribe(response => {
        if (response.status === 200) {
          custhis.categories = JSON.parse(response['_body'])[0];
          custhis.subCategories = JSON.parse(response['_body'])[1];
          custhis.details = JSON.parse(response['_body'])[2];
        }
      });
    })
  }

  filterSubCategories(id) {
    return this.subCategories.filter(x => x.category_id === id);
  }

  filterDetails(id, type) {
    if (type == 0) {
      return this.details.filter(x => x.category_id === id && x.subcategory_id == 0);
    } else {
      return this.details.filter(x => x.subcategory_id === id);
    }
  }

  ionViewDidLoad() {
    this.admobFree.banner.remove();
    this.showBanner();
  }
  showBanner() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('banner_android');
    } else if (this.platform.is('ios')) {
      adId = this.cookieService.get('banner_ios');
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    };
    this.admobFree.banner.config(bannerConfig);
    this.admobFree.banner.prepare();
  }


  showInterstitial() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('interstitial_android');
    } else if (this.platform.is('ios')) {
      adId =  this.cookieService.get('interstitial_ios');
    }
    const interstitialConfig: AdMobFreeInterstitialConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    };
    this.admobFree.interstitial.config(interstitialConfig);
    this.admobFree.interstitial.prepare();
  }

  showReward() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('rewarded_android');
    } else if (this.platform.is('ios')) {
      adId = this.cookieService.get('rewarded_ios');
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    }
    this.admobFree.rewardVideo.config(bannerConfig);
    if (this.admobFree.rewardVideo.isReady()) {
      this.admobFree.rewardVideo.prepare().then();
    } else {
      this.showInterstitial();
    }
  }

}
