import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SmartphonePage } from './smartphone';

@NgModule({
  imports: [
    IonicPageModule.forChild(SmartphonePage),
  ],
})
export class SmartphonePageModule {}
