import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CategoryPage } from '../pages/category/category';
import { SmartphonePage } from '../pages/smartphone/smartphone';
import {CommonService} from "../common.service";
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient} from '@angular/common/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CookieService  } from 'ngx-cookie-service';
import {AdMobFree} from '@ionic-native/admob-free';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CategoryPage,
    SmartphonePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CategoryPage,
    SmartphonePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CommonService,
    HttpClientModule,
    HttpClient,
    SmartphonePage,
    CategoryPage,
    HttpModule,
    CookieService,
    AdMobFree,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
