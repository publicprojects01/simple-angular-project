import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {HomePage} from '../pages/home/home';
import {CategoryPage} from '../pages/category/category';
import {CommonService} from "../common.service";
import {MenuController} from 'ionic-angular';
import {CookieService} from "ngx-cookie-service";
import {GLOBAL_CONFIG} from "../my-config/global-config";
import {AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig} from "@ionic-native/admob-free";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  models: Array<{ title: string, component: any }>;

  constructor(private admobFree: AdMobFree, private cookieService: CookieService, private menu: MenuController, private commonService: CommonService, public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.commonService.fetchAdmobInfo();
    this.initializeApp();
    this.models = [];
    this.commonService.models().subscribe(response => {
      if (response.status === 200) {
        this.models = JSON.parse(response['_body']);
      }
    });
  }

  goToHome() {
    this.nav.setRoot(HomePage);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      if (this.cookieService.get('adcounter').length <= 0) {
        this.cookieService.set('adcounter', '1')
      }
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  model(page) {
    this.nav.push(CategoryPage, {categoryName: page.name, categoryId: page.id});
     if (parseInt(this.cookieService.get('adcounter')) == 15) {
      this.showReward();
      this.cookieService.set('adcounter', '1')
    } else if (parseInt(this.cookieService.get('adcounter')) % 5 == 0) {
      this.showInterstitial();
    }
    this.cookieService.set('adcounter', (parseInt(this.cookieService.get('adcounter')) + 1).toString());
  }

  goHome() {
    this.nav.setRoot(HomePage);
    this.menu.close();
  }

  showInterstitial() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('interstitial_android');
    } else if (this.platform.is('ios')) {
      adId =  this.cookieService.get('interstitial_ios');
    }
    const interstitialConfig: AdMobFreeInterstitialConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    };
    this.admobFree.interstitial.config(interstitialConfig);
    this.admobFree.interstitial.prepare();
  }

  showReward() {
    let adId;
    if (this.platform.is('android')) {
      adId = this.cookieService.get('rewarded_android');
    } else if (this.platform.is('ios')) {
      adId = this.cookieService.get('rewarded_ios');
    }
    const bannerConfig: AdMobFreeBannerConfig = {
      id: adId,
      isTesting: GLOBAL_CONFIG.isTesting,
      autoShow: true
    }
    this.admobFree.rewardVideo.config(bannerConfig);
    if (this.admobFree.rewardVideo.isReady()) {
      this.admobFree.rewardVideo.prepare().then();
    } else {
      this.showInterstitial();
    }
  }
}
