import { Http} from '@angular/http';
import { Injectable } from '@angular/core';
import {CookieService} from "ngx-cookie-service";
@Injectable()
export class CommonService {
  // url='http://localhost:8084/SmartphoneAzWebservice/';
  url='http://mobile.smartphone.az/';
  constructor(private cookieService:CookieService,private http:Http) {

  }
  smartphoneList(page) {
    return this.http.request(this.url+"mobile/service/smartphoneList/"+page);
  }
  models() {
    return this.http.request(this.url+"mobile/service/models");
  }
  smartphoneListByModel(id,page) {
    return this.http.request(this.url+"mobile/service/smartphoneListByModel/"+id+"/"+page);
  }

  categories(){
    return this.http.request(this.url+"mobile/service/categories");
  }
  fetchAdmobInfo(){
    this.http.request(this.url+"mobile/service/admobInfo").subscribe(response => {
      if (response.status === 200) {
        this.cookieService.set('banner_android',JSON.parse(response['_body']).banner_android);
        this.cookieService.set('interstitial_android',JSON.parse(response['_body']).interstitial_android);
        this.cookieService.set('rewarded_android',JSON.parse(response['_body']).rewarded_android);
      }
    });
  }
  subCategories(){
    return this.http.request(this.url+"mobile/service/subCategories");
  }

  details(id) {
    return this.http.request(this.url+"mobile/service/details/"+id);
  }
  search(keyword,page) {
    return this.http.request(this.url+"mobile/service/search/"+keyword+"/"+page);
  }

}
